aws s3  cp template.yaml  s3://pennidinh-code-assets/template.yaml

aws cloudformation update-stack --region us-west-2 --stack-name PenniDinh-Cloud --capabilities CAPABILITY_NAMED_IAM --template-url https://pennidinh-code-assets.s3-us-west-2.amazonaws.com/template.yaml
